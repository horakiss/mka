#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#MKA:xhorak61

##
# @file automaton.py
# @brief The automaton class
# @date 5.4.2016
# @author Jakub Horák, xhorak61@stud.fit.vutbr.cz

from settings import *

##
# @class Auto
# @brief The Auto class represents automaton
# @details The auto class contains data of automaton and methods
class Auto :

    ## Group of automaton's states
    __state = []
    ## Group of automaton's alphabeta
    __alpha = []
    ## Group of automaton's rules
    __rules = []
    ## Start's state of automaton
    __start = None
     ## Group of automaton's finishing states
    __finish = []
    ## Non finishing state if exists
    __fnf = None



    ##
    # @brief    Add state to group of states
    # @param    name The name of state
    #
    # @details  Don't add state, if state is in group of states
    @staticmethod
    def addState(name) :
        if not name in Auto.__state :
            Auto.__state.append(name)

    ##
    # @brief    Add char to group of alphabeta
    # @param    name The character
    #
    # @details  Dont't add character, if it is in group of alphabeta
    @staticmethod
    def addAlpha(name) :
        if not name in Auto.__alpha :
            Auto.__alpha.append(name)

    ##
    # @brief   Add state/alpha/state to group of rules
    # @param   name The name of state or character
    # @param   num The number: 0 - state, 1 - alphabeta
    #
    # @details  Rules are saved in array in format:
    #           ar[i] = state / ar[i+i] = alpha / ar[i+2] = state
    #           This represnt p 'a' -> q
    @staticmethod
    def addRules(name, num) :
        if num == 0 and not name in Auto.__state :  # If state isn't defined
            Settings.error(61, "Error: Undefined state in rules.\n")
        elif num == 1 and not name in Auto.__alpha and name != "eps": # If alpha isn't defined
            Settings.error(61, "Error: Undefined alpha in rules.{}\n".format(name))

        Auto.__rules.append(name)

    ##
    # @brief    Add start state
    # @param    name The name of start state
    #
    # @details  Function check if start is defined once and state is defined
    @staticmethod
    def addStart(name) :
        if Auto.__start == None : # Is it first use
            if name in Auto.__state : # Is state defined
                Auto.__start = name
            else:
                Settings.error(61, "Error: Start state is undefined state.\n")
        else :
            Settings.error(62, "Error: You can declare just one start state\n")


    ##
    # @brief    Add state to group of finishing states
    # @param    name The name of inserting state
    #
    # @details  Don't insert new state, if it is in array
    @staticmethod
    def addFinish(name) :
        if name in Auto.__state :
            Auto.__finish.append(name)
        else :
            Settings.error(61, "Error: Finish state is undefined state.\n")

    ##
    # @brief    Check if automaton is DSKA
    #
    # @details  Parts of checking:
    #           1. Rules hasn't epsilon state
    #           2. Automaton hasn't nondeterministic states
    #           3. Automaton hasn't nonreachable states
    #           4. Automaton has max one nonfinishing states
    @staticmethod
    def checkAuto() :
        Auto.isEpsilon()
        Auto.isNedeterm()
        Auto.isNotreach()
        Auto.fnf()
        Auto.__alpha.sort()

    ##
    # @brief    Check if rules consists epsilon in alphabeta
    #
    @staticmethod
    def isEpsilon() :
        for i in range(1, len(Auto.__rules), 3) :
            if Auto.__rules[i] == "eps" :
                Settings.error(62, "Error: Epsilon in rules.\n")

    ##
    # @brief    Check if automaton is deterministic
    #
    # @details  Copy rules to tmp_array and check if rules hasnt same
    #           alphabeta and destination state
    @staticmethod
    def isNedeterm() :
        ## Temporary array for save rules
        tmp_arr = []
        tmp_arr.append(Auto.__rules[0]) ## Save state
        tmp_arr.append(Auto.__rules[1]) ## Save alphabeta
        tmp_arr.append(Auto.__rules[2]) ## Save destination state

        ## For each rule in __rules
        for i in range(3, len(Auto.__rules), 3) :
            ## For each rule in tmp_arr
            for y in range(0, len(tmp_arr), 3) :
                ## Compare states
                if Auto.__rules[i] == tmp_arr[y] :
                    ## Compare alphabetas
                    if Auto.__rules[i+1] == tmp_arr[y+1]:
                        ## Compare destination states
                        if Auto.__rules[i+2] != tmp_arr[y+2]:
                            Settings.error(62, "Error: Nondeter Automaton.\n")
                ## If not reach error, save rule to tmp_arr
                tmp_arr.append(Auto.__rules[i])
                tmp_arr.append(Auto.__rules[i+1])
                tmp_arr.append(Auto.__rules[i+2])

    ##
    # @brief    Check if automaton consists nonreachable states
    #
    # @details
    @staticmethod
    def isNotreach() :
        ## Set start state to local variable
        start = Auto.__start
        ## Temporary array for save rules
        tmp_arr = []

        ## For each destination states in group of rules
        for i in range(2, len(Auto.__rules), 3) :
            ## Compare if state isn't destination state
            if Auto.__rules[i-2] != Auto.__rules[i] :
                ## Compare if destination rules in tmp_arr
                if not Auto.__rules[i] in tmp_arr :
                    ## If isn't add rule to tmp_arr
                    tmp_arr.append(Auto.__rules[i])

        ## For each state in group of rules
        for i in range(0, len(Auto.__rules), 3) :
            ## If not state in destination states of rules
            if not Auto.__rules[i] in tmp_arr :
                ## And not start state
                if Auto.__rules[i] != start :
                        Settings.error(62, "Error: Unreacheble state.\n")

    ##
    # @brief Find non finishing state
    #
    #
    @staticmethod
    def fnf() :

        ## Temporary array for states which are finishing
        end_states = []

        ## Copy all states from group of finishing states
        for i in range(0, len(Auto.__finish), 1):
            end_states.append(Auto.__finish[i])

        ## Variable for end cycle of finding non finishing states
        isEnd = True
        while isEnd :
            isEnd = False
            ## For each state in group of finishing states
            for i in range(0, len(end_states), 1) :
                ## For each state in group of rules
                for y in range(0, len(Auto.__rules), 3) :
                    ## If destination rules is in end states
                    if Auto.__rules[y+2] in end_states :
                        ## And state isn't in end states
                        if not Auto.__rules[y] in end_states :
                            ## Add state to end states
                            end_states.append(Auto.__rules[y])
                            isEnd = True

        ## For each state in group of rules
        for i in range(0, len(Auto.__rules), 3):
            ## Load state
            state = Auto.__rules[i]
            ## If state isn't in end states
            if not state in end_states :
                ## If it is first non finishing state or same
                if Auto.__fnf == None or Auto.__fnf == state:
                    Auto.__fnf = state
                else :
                    Settings.error(62, "Error: More than two unreach state.\n")

    @staticmethod
    def getNonFinSt() :
        nEndSt = []
        for i in range(0, len(Auto.__state), 1) :
            if not Auto.__state[i] in Auto.__finish :
                nEndSt.append(Auto.__state[i])
        return nEndSt

    ##
    # @brief    Get non finishing state of automaton
    # @return   Auto.__fnf non finish state
    @staticmethod
    def getFnf() :
        return Auto.__fnf

    @staticmethod
    def printAutomata():
        automata = "(\n{"
        ## States
        tmp_arr = [];
        for subset in Auto.__state:
            tmp_arr.append(subset)
        tmp_arr.sort()

        for state in tmp_arr :
            automata += state + ", "

        automata = automata[:-2]
        automata += "},\n{"

        ## Alphabet
        alpha = Auto.getAlphaSet()
        alpha.sort()
        for char in alpha :
            automata += "'{}', ".format(char)

        automata = automata[:-2]
        automata += "},\n{\n"

        ## Rules
        rules = ""
        tmp_rules = []
        for i in range(0, len(Auto.__rules), len(Auto.__alpha)+1) :
            if not Auto.__rules[i] in tmp_rules :
                tmp_rules.append(Auto.__rules[i])
        tmp_rules.sort()

        for i in range(0, len(tmp_rules), 1) :
            for alpha in Auto.__alpha:
                for y in range(0, len(Auto.__rules), len(Auto.__alpha)+1) :
                    if tmp_rules[i] == Auto.__rules[y] :
                        if alpha == Auto.__rules[y+1] :
                            automata += tmp_rules[i] + " '" + alpha + "' -> "
                            automata += Auto.__rules[y+2] + ",\n"
                            break

        automata = automata[:-2]
        automata += "\n"
        automata += "},\n"

        ## Start state
        automata += Auto.__start + ",\n{"

        ## End States
        for end_st in Auto.__finish :
            automata += end_st + ", "

        automata = automata[:-2]
        automata += "}\n)"

        return automata

    @staticmethod
    def getAlphaSet() :
        return Auto.__alpha

    @staticmethod
    def getStartSt() :
        return Auto.__start

    @staticmethod
    def getFinishSet() :
        return Auto.__finish

    @staticmethod
    def getRulesSet() :
        return Auto.__rules

    @staticmethod
    def getStateSet() :
        return Auto.__state



