#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#MKA:xhorak61

##
# @file data.py
# @brief The Data class
# @date 22.3.2016
# @author Jakub Horák, xhorak61@stud.fit.vutbr.cz

from settings import *
from automaton import *

##
# @class Data
# @brief The Data class represents lexical and syntatic parser
# @details The Files class contains automata for parsing input file
class Data :
    ## Input file saved as string
    __data = None
    ## Length of data
    __data_length = 0
    ## Actual pointer
    __pointer = 0
    ## Actual state of main automata
    __arr_states = None

    ##
    # @brief Set data
    # @param string The input automata
    @staticmethod
    def setData(contants) :
        """ Method load string to data """
        Data.__data = contants
        Data.__data_length = len(Data.__data)

    ##
    # @brief Set data to lowercase
    #
    @staticmethod
    def setLower() :
        if Data.__data != None :
            Data.__data = Data.__data.lower()

    ##
    # @brief Start parse data
    @staticmethod
    def parse_automaton() :

        auto_start = False
        auto_end = False
        state = 0
        group_sep = True
        c = Data.getDataChar()
        while c != -1:
            if c in ("\n", "\r", "\t", " ") :
                c = Data.getDataChar()
                continue
            elif c == "#" :
                Data.parse_com()
            elif c == "(" :
                auto_start = True
            elif c == "," :
                if not group_sep :
                    group_sep = True
                else :
                    Settings.error(60, "Error: Syntax error in automan. Missing separator between groups\n")
            elif c == "{" :
                if group_sep :
                    state = Data.find_state(state)
                    group_sep = False
                else :
                    Settings.error(60, "Error: Missing separator between groups\n")
            elif c == ")" :
                if auto_start and state == 5:
                    Data.parse_end_auto()
                    auto_end = True
                    break   # Now we get valid Auto
            elif c.isalpha() :
                if state == 3 :
                    Data.parse_start()
                    state += 1
                else :
                    Settings.error(60, "Error: Syntax error in automan2.\n")
            else :
                Settings.error(60, "Error: Syntax error in automan1.\n")
            c = Data.getDataChar()

        if auto_end :
            return
        else :
            Settings.error(60,"Error: Wrong ended Auto. Missing ')'\n")

    ##
    # @brief Handle end of file after automata definition
    #
    @staticmethod
    def parse_end_auto() :
        """ Method handle end of file after Auto declaration """
        c = Data.getDataChar()
        while c != -1:
            if c in ("\n", "\r", "\t", " ") :
                c = Data.getDataChar()
                continue
            elif c == "#" :
                Data.parse_com()
            elif c == -1 :
                return True
            else :
                Settings.error(60, "Bad ended end of Auto")
            c = Data.getDataChar()

    ##
    # @brief Choose main automata state
    # @param int Actual state
    @staticmethod
    def find_state(state) :
        if state == 0 :
            Data.parse_states()
        elif state == 1 :
            Data.parse_alphabeta()
        elif state == 2 :
            Data.parse_rules()
        elif state == 4 :
            Data.parse_finish()

        state += 1
        return state

    ##
    # @brief Parse set of finish states
    #
    @staticmethod
    def parse_finish() :
        iden = False
        c = Data.getDataChar()
        while c != -1 :
            if c in ("\n", "\r", "\t", " ") :
                c = Data.getDataChar()
                continue
            elif c == "#" :
                Data.parse_com()
            elif c == "," :
                if iden :
                    iden = False
                else :
                    Settings.error(60,"Error: Wrong defined group of finish states\n")
            elif c == "}" :
                if iden :
                    return
                else :
                    if len(Auto.getFinishSet()) == 0:
                        Settings.error(62, "Error: Empty set of finish states")
                    else :
                        Settings.error(60,"Error: Wrong defined group of finish states\n")
            elif c.isalpha :
                Auto.addFinish( Data.load_name(c) )
                iden = True
            else :
                Settings.error(60,"Error: Wrong defined group of finish states\n")
            c = Data.getDataChar()

    ##
    # @brief Parse start state
    #
    @staticmethod
    def parse_start() :
        just_once = False
        Data.decPointer()
        c = Data.getDataChar()
        while c != -1 :
            if c in ("\n", "\r", "\t", " ") :
                c = Data.getDataChar()
                continue
            elif c == "#" :
                Data.parse_com()
            elif c == "," :
                return
            elif c.isalpha :
                if not just_once :
                    Auto.addStart( Data.load_name(c) )
                    just_once = True
                else :
                    Settings.error(60,"Error: You can define just one start state.\n")
            else :
                Settings.error(60,"Error: Wrong defined group of starts\n")
            c = Data.getDataChar()

    ##
    # @brief Parse set of states
    #
    @staticmethod
    def parse_states() :
        iden = False
        first = True
        c = Data.getDataChar()
        while c != -1 :
            if c in ("\n", "\r", "\t", " ") :
                c = Data.getDataChar()
                continue
            elif c == "#" :
                Data.parse_com()
            elif c == "," :
                if iden :
                    iden = False
                else :
                    Settings.error(60,"Error: Wrong defined group of states\n")
            elif c == "}" :
                if iden :
                    return
                else :
                    if len(Auto.getStateSet()) == 0 :
                        Settings.error(61,"Error: Empty set of states\n")
                    else :
                        Settings.error(60,"Error: Wrong defined set of states\n")
            elif c.isalpha :
                Auto.addState( Data.load_name(c) )
                iden = True
            else :
                Settings.error(60,"Error: Wrong defined group of states\n")
            c = Data.getDataChar()

    ##
    # @brief Parse set of rules
    #
    @staticmethod
    def parse_rules() :
        state = 0
        c = Data.getDataChar()
        while c != -1 :
            if c in ("\n", "\r", "\t", " ") :
                c = Data.getDataChar()
                continue
            elif c == "#" :
                Data.parse_com()
            elif state == 0 : # Nacitam stav
                if c.isalnum() :
                    Auto.addRules( Data.load_name(c), 0 )
                    state = 1
                elif c in ("\n", "\r", "\t", " ") :
                    c = Data.getDataChar()
                    continue
                else :
                    Settings.error(60,"Error: Error in rules.\n")
            elif state == 1 : # Nacitam abecedu
                if c == "\'" :
                    Auto.addRules(Data.loadAlpha(1),1)

                    state = 2
                elif c in ("\n", "\r", "\t", " ") :
                    c = Data.getDataChar()
                    continue
                else :
                    Settings.error(60,"Error: Alphabeta in rules.\n")
            elif state == 2 : # Load arrow
                if c == "-" :
                    c = Data.getDataChar()
                    if c == ">" :
                        state = 3
                    else :
                       Settings.error(60,"Error: Need arrow in rule.\n")
                elif c in ("\n", "\r", "\t", " ") :
                    c = Data.getDataChar()
                    continue
                else :
                    Settings.error(60,"Error: Need arrow in rule.\n")
            elif state == 3 : # Nacitam stav
                if c.isalnum() :
                    Auto.addRules( Data.load_name(c),0)
                    state = 4
                elif c in ("\n", "\r", "\t", " ") :
                    c = Data.getDataChar()
                    continue
                else :
                    Settings.error(60,"Error: Load next state.\n")
            elif state == 4 : # Load next rule or end of set of rules
                if c == "," :
                    state = 0
                elif c == "}" :
                    return
                elif c in ("\n", "\r", "\t", " ") :
                    c = Data.getDataChar()
                    continue
                else :
                    Settings.error(60,"Error: Wrong ended rule.\n")
            c = Data.getDataChar()

    ##
    # @brief Parse set of alphabetas
    # @param int The number where is loaded 0 = set of alphabeta , 1 = rules
    # @return string The loaded alphabeta
    #
    # @details When is loaded epsilon state '', methods return string eps
    @staticmethod
    def loadAlpha(cmd) :
        c = Data.getDataChar()
        result = ""
        if c == "\'":
            if Data.getDataChar() == "\'":
                result = "''"
            else :
                result = "eps"
                Data.decPointer()
        else :
            result = c
        if Data.getDataChar() != "\'" :
            if cmd == 0 :
                Settings.error(60,"Error: Wrong declared alphabeta.\n")
            else :
                Settings.error(62,"Error: Epsilon in rules.\n")
        return result

    ##
    # @brief Parse set of alphabet
    #
    @staticmethod
    def parse_alphabeta() :
        """ Method parse group Alphabeta """
        comma = False
        first = False
        c = Data.getDataChar()
        while c != -1 :
            if c in ("\n", "\r", "\t", " ") :
                c = Data.getDataChar()
                continue
            elif c == "#" :
                Data.parse_com()
            elif c == "," :
                if comma :
                    comma = False
                else :
                    Settings.error(60,"Error: Wrong defined group of alpha 1\n")
            elif c == "\'":
                al_res = Data.loadAlpha(0)
                if al_res == "eps" :
                    Settings.error(62,"Error: Epsilon in alphabeta\n")
                Auto.addAlpha(al_res)
                first = True
                comma = True
            elif c == "}" :
                if comma :
                    return
                else :
                    if len(Auto.getAlphaSet()) == 0 :
                        Settings.error(61,"Error: Empty alphabet\n")
                    else :
                        Settings.error(60,"Error: Wrong defined alphabet\n")

            c = Data.getDataChar()

    ##
    # @brief Parse comment
    @staticmethod
    def parse_com() :
        c = Data.getDataChar()
        while c != -1 :
            if c == '\n' :
                return
            c = Data.getDataChar()

    ##
    # @brief Parse name of state
    # @param string The first number of name
    # @return string The name of state
    @staticmethod
    def load_name(char) :
        tmp_string = char
        c = Data.getDataChar()
        while c != -1 :
            if c.isalnum() or c == "_":
                tmp_string += c
            else :
                Data.decPointer()
                if tmp_string.endswith("_") :
                    Settings.error(60, "Error: Name of state can't start with '_'\n")
                return tmp_string
            c = Data.getDataChar()

    ##
    # @brief Get character from data
    # @return string The actual character of data or -1 when is EOF
    @staticmethod
    def getDataChar() :
        pointer = Data.getPointer()
        if pointer != -1:
            Data.incPointer()
            return Data.__data[pointer]
        else :
            return -1

    ##
    # @brief Get actual pointer of data
    # @return int The pointer to string or -1 when is EOF
    @staticmethod
    def getPointer() :
        """ Method return actual pointer """
        if Data.__pointer < Data.__data_length :
            return Data.__pointer
        else :
            return -1

    ##
    # @brief Increase pointer of data
    @staticmethod
    def incPointer() :
        Data.__pointer += 1

    ##
    # @brief Decrease pointer of data
    @staticmethod
    def decPointer() :
        Data.__pointer -= 1
