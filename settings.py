#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#MKA:xhorak61

##
# @file settings.py
# @brief The settings of program
# @date 22.3.2016
# @author Jakub Horák, xhorak61@stud.fit.vutbr.cz

import sys

##
# @class Settings
# @brief The Settings class represents settings of program
# @details Contains data of settings and check valid set settings
class Settings :
    ## Find non finish trigger
    fnf = False
    ## Minimize trigger
    minimize = False
    ## Case-insensitive trigger
    case = False

    ##
    # @brief Check if user don't use parameter -m and -f
    # @return boolean True if parameters is ok, False if not
    @staticmethod
    def checkSettings() :
        if Settings.minimize == True or Settings.fnf == True:
            return False
        else :
            return True

    ##
    # @brief Set trigger find non finishing
    #
    # @details If checkSettings() return False mean that was parameter used
    #          twice or was used wrong combination of parameters
    @staticmethod
    def setFNF() :
        """ Method set fnf if parameter -f, --find-non-finishing was used """
        if Settings.checkSettings() :
            Settings.fnf = True
        else :
            Settings.error(1, 'Chyba find non finishing\n')

    ##
    # @brief Get value of fnf
    # @return boolean True - was used, False - wasn't used
    @staticmethod
    def getFNF() :
        return Settings.fnf

    ##
    # @brief Set trigger minimize
    #
    # @details If checkSettings() return False mean that was parameter used
    #          twice or was used wrong combination of parameters
    @staticmethod
    def setMinimize() :
        if Settings.checkSettings() :
            Settings.minimize = True
        else :
            Settings.error(1, 'Chyba minimize\n')

    ##
    # @brief Get value of minimize
    # @return boolean True - was used, False - wasn't used
    @staticmethod
    def getMinimize() :
        """ Method return True if parameter -m, --minimize is set """
        return Settings.minimize

    ##
    # @brief Set trigger case
    #
    # @details If was used twice, call error()
    @staticmethod
    def setCase() :
        """ Method set parameter -i, --case_insensitive """
        if Settings.case == False:
            Settings.case = True
        else :
            Settings.error(1, 'Chyba case insensitive\n')

    ##
    # @brief Get value of case
    # @return boolean True - was used, False - wasn't used
    @staticmethod
    def getCase() :
        """ Method return True if parameter -i, --case_insensitive is not set """
        return Settings.case

    ##
    # @brief Print error message and return exit code
    # @param int The number of error code
    # @param string The information about error
    @staticmethod
    def error(error_num, error_info) :
        """ This method print reason of error and exit program """
        sys.stderr.write(error_info)
        sys.exit(error_num)

    ##
    # @brief Print help to stdout
    @staticmethod
    def print_help():
        """ Method print help of program to stdout """
        help = """Help page of mka.jsn
Author: Jakub Horák
Email: xhorak61@stud.fit.vutbr.cz
\nSYNOPSIS
\tmka.py [ -h ] [ --help ] [ -f ] [ --find-non-finishing ] [ -m ] [ --minimize ] [ -i ] [ --case_insensitive] [ --input filename ] [ --output filename]
\nDESCRIPTION
\tScript make minimalization of finishing automaton
\nCOMMAND LINE OPTIONS
\t-h --help\n\t\t Print this man page of program.\n
\t-f --find-non-finishing\n\t\t Find non finishing states\n
\t-m --minimize\n\t\t Make minimaze of automaton\n
\t-i --case_insensitive\n\t\t Ignore case sensitive of states"""

        print(help)
        sys.exit(0)
