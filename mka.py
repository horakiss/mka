#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#MKA:xhorak61

# Modul: Main class
# Project: MKA - Minimalizace konecneho automatu
# Author: Jakub Horák
# Login: xhorak61
# Date: 22.3.2016

import getopt, sys

from settings import *
from files import *
from data import *
from automaton import *
from automata_min import *

try:
    short_opts = "hfmi"
    long_opts = ["help", "find-non-finishing", "case-insensitive", "minimize", "input=", "output="]
    opts, args = getopt.getopt(sys.argv[1:], short_opts, long_opts)
except getopt.GetoptError as err:
    Settings.error(1, 'Error: Undefined options\n')

for opt, arg in opts:
    if opt in ("-h", "--help") :
         ## Tady udělej kontrolu parametrů help
        if len(opts) == 1:
            Settings.print_help()
        else :
            Settings.error(1,"Error: Used opt with --help or used twice\n")

for opt, arg in opts:
    if opt in ("-f", "--find-non-finishing") :
        Settings.setFNF()
    elif opt in ("-m" , "--minimize") :
        Settings.setMinimize()
    elif opt in ("-i", "--case-insensitive") :
        Settings.setCase()
    elif opt == "--input" :
        Files.load_input(arg)
    elif opt == "--output" :
        Files.setOutput(arg)
    else:
        assert False, "unhandled option"



if Files.getInput() == None:
    automata = ""
    for data in sys.stdin:
        automata += data
    Data.setData(automata)

if Settings.case == True:
    Data.setLower()

Data.parse_automaton()
Auto.checkAuto()

if Settings.getFNF() == True :
    Files.write_output(Auto.getFnf())
elif Settings.getMinimize() == True :
    Auto_min.minimization()
    Auto_min.printMinAutomata()
    Files.write_output(Auto_min.getMinAutomata())
else :
    Files.write_output(Auto.printAutomata())


exit(0)
