#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#MKA:xhorak61

##
# @file files.py
# @brief The Files class
# @date 22.3.2016
# @author Jakub Horák, xhorak61@stud.fit.vutbr.cz

from settings import *
from data import *

##
# @class Files
# @brief The Files class represents input/output class
# @details The Files class contains information about i/o files and methods
class Files :
    ## Handler of input file
    __input_file = None
    ## Handler of ouput file
    __output_file = None
    ## Output file path
    __output_path = None

    ##
    # @brief Load input data to string
    # @param string The input file path
    @staticmethod
    def load_input(path) :
        """ Method load input to Data.setData """
        if Files.__input_file != None :
            Settings.error(1, 'Error: Duplicit input')
        try:
            Files.__input_file = open(path)
            Data.setData(Files.__input_file.read())
            Files.__input_file.close()
        except IOError:
            Settings.error(2, 'Error: Can\'t open file: \"%s\" .\n' % path)

    ##
    # @brief Set output path
    # @param string The output path
    @staticmethod
    def setOutput(path) :
        if Files.__output_path != None :
            Settings.error(1, 'Error: Duplicit output')
        """ Method save path of output file """
        Files.__output_path = path
        Files.load_output()
        Files.__output_file.close()

    ##
    # @brief Get output path
    # @return string The output path
    @staticmethod
    def getOutput() :
        """ Method return output file path """
        return Files.__output_path

    ##
    # @brief Open file for write
    # @details If file contains data, data will be rewrite
    @staticmethod
    def load_output() :
        """ Method prepare descriptor of output file """
        try:
            Files.__output_file = open(Files.__output_path, 'w')
        except IOError:
            Settings.error(3, 'Error: Can\'t open file: \"%s\" .\n' % Files.__output_path)

    ##
    # @brief Print result of program
    # @param string The result of program
    @staticmethod
    def write_output(result) :
        """ Method writes result to file or STDOUT """
        if Files.getOutput() != None :
            Files.load_output();
            Files.__output_file.write(result)
            Files.__output_file.close()
        else :
            print(result, end="")

    ##
    # @brief Get the input file
    # @return int The descriptor of input file
    @staticmethod
    def getInput():
        return Files.__input_file



