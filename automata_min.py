#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#MKA:xhorak61

##
# @file automata_min.py
# @brief The automaton minimization class
# @date 16.4.2016
# @author Jakub Horák, xhorak61@stud.fit.vutbr.cz

from automaton import *

##
# @class Auto_min
# @brief The Auto_min class represents minimization automaton
# @details The auto_min class contains data of min automaton and methods for
#          minimization automata
class Auto_min(Auto):

    ## Set of minimization states
    __qm = []
    ## Set of minimazation rules
    __rm = []
    ## Minimization start
    __sm = []
    ## Set of minimazation finish states
    __fm = []
    ## Concaneted set of minimazion rules
    __result = ""
    ## Ordered min rules
    __min_rules = []

     ##
    # @brief Minimization automata
    #
    #
    @staticmethod
    def minimization() :
        Auto_min.getOrderRules()
        qm_arr = [Auto.getFinishSet(),Auto.getNonFinSt()]
        alphabeta = Auto.getAlphaSet()

        ## For every subset X of set Qm
        divide = True
        while divide :
            divide = False
            for x_arr in qm_arr :
                ## For every alpha d of set Alphabeta
                for d in range(0, len(alphabeta), 1) :
                    ## For every state of set X
                    x1 = []
                    x2 = []
                    ref = Auto_min.getSet(qm_arr,Auto_min.getDest(x_arr[0],d))
                    for x_st in x_arr :
                        # Get dest state of x_st with alpha d
                        dest = Auto_min.getDest(x_st,d)
                        # Get index from array qm_arr
                        index = Auto_min.getSet(qm_arr, dest)
                        # Add x_st if dest is from same array in qm_arr
                        if ref == index :
                            x1.append(x_st)
                        else :
                            x2.append(x_st)
                    # If any x_st wasnt from same => divide
                    if len(x2) != 0 :
                        divide = True
                        qm_arr = Auto_min.divide(qm_arr,x1)

        Auto_min.__qm = qm_arr
        Auto_min.makeMinAutomata()

    ##
    # @brief Make divide in array qm
    # @param [] The original qm array of arrays
    # @param [] The array which divide from qm
    # @return [] The divided qm array
    @staticmethod
    def divide(qm,x1) :
        tmp_arr = []
        # For every array in qm
        for i in range(0, len(qm), 1) :
            # If first state of x1 in array
            if x1[0] in qm[i] :
                tmp_arr.append(x1)
                # Get states of qm[i] which now in x1
                diff = Auto_min.getDiff(x1,qm[i])
                if len(diff) != 0 :
                    tmp_arr.append(diff)
            else :
                tmp_arr.append(qm[i])
        return tmp_arr

    ##
    # @brief Get states which are in set and not in x1
    # @param [] The array of states which aren't wanted
    # @param [] The array of states
    # @return [] The array of states which are in set and not in x1
    @staticmethod
    def getDiff(x1,my_set):
        tmp_arr = []
        for state in my_set :
            if not state in x1:
                tmp_arr.append(state)
        return tmp_arr

    ## Get index of qm array, founded with state
    # @param [] The array of states
    # @param [] State which is key to find array
    # @return int index of array in qm
    @staticmethod
    def getSet(qm, state) :
        for index in range(0, len(qm), 1) :
            if state in qm[index] :
                return index

    ##
    # @brief Get destination state of rule
    # @param string The rule's state
    # @param int The index to alphabeta
    # @return string The destination state
    @staticmethod
    def getDest(state,d) :
        states = Auto.getRulesSet()
        alphabeta = Auto.getAlphaSet()

        for i in range(0, len(states), 3) :
            if state == states[i] :
                if alphabeta[d] == states[i+1] :
                    return states[i+2]

    ##
    # @brief Get concaneted minimization rules
    # @return string Every rules is on the new line
    #
    # @details Format: state 'alphabeta' -> destination,
    @staticmethod
    def getMinAutomata() :
        return Auto_min.__result

    ##
    # @brief Make arrays of minimization automata
    #
    # @details Transform arrays of Auto to Auto_min's arrays
    @staticmethod
    def makeMinAutomata() :
        qm_arr = Auto_min.__qm
        rm_arr = []
        sm_result = None
        fm_result = []

        # Create array of rules
        # Format: state dest'n' dest'n+1'
        for subset in qm_arr :
            rm_arr.append(subset)
            # For every alphabeta
            for alpha_index in range(0, len(Auto.getAlphaSet()), 1) :
                tmp_dest = [] ##< array of destination states of subset
                # For every state in subset
                for state in subset :
                    # Get destination state and add it
                    dest =  Auto_min.getDest(state,alpha_index)
                    if not dest in tmp_dest :
                        tmp_dest.append(dest)
                # Append all destination states
                rm_arr.append(tmp_dest)

        tmp_arr = []
        for i in range(0,len(rm_arr), 1):
            for state in qm_arr:
                # If state is subset, replace it
                if rm_arr[i][0] in state:
                    tmp_arr.append(state)
        rm_arr = tmp_arr

        # Find start state
        for subset in qm_arr :
            if Auto.getStartSt() in subset :
                sm_result = subset
                break

        # Make finishing states
        for end_st in Auto.getFinishSet() :
            for subset in qm_arr :
                if end_st in subset :
                    if not subset in fm_result :
                        fm_result.append(subset)
                    break


        Auto_min.__rm = rm_arr
        Auto_min.__sm = sm_result
        Auto_min.__fm = fm_result

    ##
    # @ brief Concate array of rules ordered by name
    # @param array The array of states and dest states
    # @return string The string of concaneted rules
    @staticmethod
    def concatRules() :
        alpha = Auto.getAlphaSet()
        len_alpha = len(alpha)
        rm_arr = Auto_min.__rm
        result = ""

        ## Take rm_arr, take states and order them
        sort_rm = []
        for i in range (0, len(rm_arr), len_alpha + 1) :
            sort_rm.append(rm_arr[i])
            rm_arr[i].sort()
        sort_rm.sort()

        ## Find match to sorted states and print concanet rules
        for rm_set in sort_rm :
            for i in range(0, len(rm_arr), len_alpha + 1) :
                if rm_set == rm_arr[i] :
                    con_st = Auto_min.concatStates(rm_set)
                    for al in range(0, len_alpha, 1) :
                        con_dest = Auto_min.concatStates(rm_arr[i+al+1])
                        char = " '{}' -> ".format(alpha[al])
                        result += con_st + char + con_dest + ",\n"
                    break
        result = result[:-2]
        result += "\n"
        return result


    ##
    # @brief Function concate array of states ordered by name
    # @param array The array of states
    # @return string The concated states in one name with "_"
    @staticmethod
    def concatStates(st_arr) :
        con_state = ""
        st_arr.sort()
        for state in st_arr :
            if len(con_state) != 0 :
                con_state += "_"
            con_state += state
        return con_state

    ##
    # @brief Print minimization automata
    #
    # @detail Format is defined in datasheet
    @staticmethod
    def printMinAutomata() :
        min_automata = "(\n{"
        ## States
        tmp_arr = [];
        for subset in Auto_min.__qm:
            tmp_arr.append(Auto_min.concatStates(subset))
        tmp_arr.sort()

        for state in tmp_arr :
            min_automata += state + ", "

        min_automata = min_automata[:-2]
        min_automata += "},\n{"

        ## Alphabet
        alpha = Auto.getAlphaSet()
        alpha.sort()
        for char in alpha :
            min_automata += "'{}', ".format(char)

        min_automata = min_automata[:-2]
        min_automata += "},\n{\n"

        ## Rules
        min_automata += Auto_min.concatRules()
        min_automata += "},\n"

        ## Start state
        min_automata += Auto_min.concatStates(Auto_min.__sm) + ",\n{"

        ## End States
        for end_st in Auto_min.__fm :
            min_automata += Auto_min.concatStates(end_st) + ", "

        min_automata = min_automata[:-2]
        min_automata += "}\n)"

        Auto_min.__result =  min_automata

    ##
    # @brief Order set of rules
    #
    @staticmethod
    def getOrderRules() :
        rules = Auto.getRulesSet()
        alphabeta = Auto.getAlphaSet()

        for i in range(0, len(rules), 3) :
            if not rules[i] in Auto_min.__min_rules :
                st = rules[i]
                Auto_min.__min_rules.append(st)
                Auto_min.__min_rules.append([])
                for alpha in alphabeta :
                    for y in range(0, len(rules), 3) :
                        if st == rules[y] :
                            if rules[y+1] == alpha :
                                Auto_min.__min_rules[len(Auto_min.__min_rules)-1].append(rules[y+2])
